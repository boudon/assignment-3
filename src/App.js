import React, { useEffect, useState } from 'react'
function App() {
  const [cars, setCar] = useState([])
  const [productionyear, setProductionYear] = useState("");
  const [color, setColor] = useState("");
  const [price, setPrice] = useState("");
  const [modelname, setModelName] = useState("");
  const [id,setCarId]=useState(null)

  useEffect(() => {
    getCars();
  }, [])

  function getCars() {
    fetch(`https://7bkt0kktb3.execute-api.us-east-2.amazonaws.com/cars`, {
      method: 'GET',
      headers:{
        'Accept':'application/json',
        'Content-Type':'application/json'
      },
    }).then((result) => {
      result.json().then((resp) => {
        setCar(resp.Items)
      })
    })
  }

  function deleteCar(id) {
    fetch(`https://7bkt0kktb3.execute-api.us-east-2.amazonaws.com/cars/${id}`, {
      method: 'DELETE'
    }).then((result) => {
      result.json().then((resp) => {
        //console.warn(resp)
        getCars()
      })
    })
  }


  function selectcar(id)
  {
    let item=cars[id-1];
    setProductionYear(item.productionyear)
        setColor(item.color)
        setPrice(item.price)
        setModelName(item.modelname);
        setCarId(item.id)
  }


  function updateCar()
  {
    let item={id,modelname,color,productionyear,price}
    console.warn("item",item)
    fetch(`https://7bkt0kktb3.execute-api.us-east-2.amazonaws.com/cars`, {
      method: 'PUT',
      headers:{
        'Accept':'application/json',
        'Content-Type':'application/json'
      },
      body:JSON.stringify(item)
    }).then((result) => {
      result.json().then((resp) => {
        console.warn(resp)
        getCars()
      })
    })
  }


  return (
    <div className="App">
      <h1>Cars Database </h1>
      <table border="5" style={{ float: 'center' }}>
        <tbody>
          <tr>
            <td>ID</td>
            <td>Model name</td>
            <td>Color</td>
            <td>Production year</td>
            <td>Price</td>
          </tr>
          {
            cars.map(item =>
              <tr key={item.id}>
                <td>{item.id}</td>
                <td>{item.modelname}</td>
                <td>{item.color}</td>
                <td>{item.productionyear}</td>
                <td>{item.price}</td>
                <td><button onClick={() => deleteCar(item.id)}>Delete</button></td>

              </tr>
            )
          }
        </tbody>
      </table>
      <div>
      <td>ID:</td><input type="text" value={id} onChange={(e)=>{setCarId(e.target.value)}} /> <br /><br />
      <td>Model name:</td><input type="text" value={modelname} onChange={(e)=>{setModelName(e.target.value)}} /> <br /><br />
      <td>Color:</td><input type="text" value={color} onChange={(e)=>{setColor(e.target.value)}} /> <br /><br />
      <td>Production year:</td><input type="text" value={productionyear}  onChange={(e)=>{setProductionYear(e.target.value)}} /> <br /><br />
      <td>Price:</td><input type="text" value={price} onChange={(e)=>{setPrice(e.target.value)}} /> <br /><br />
        <button onClick={updateCar} >Update Car</button>
        <td>To update an existing car make sure to use the same ID !</td>
      </div>


    </div>
  );
}
export default App;
